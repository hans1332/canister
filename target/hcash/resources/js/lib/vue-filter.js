Vue.filter('code', function(codeId, code) {
	if (typeof(code) == "string")
		code = JSON.parse(decodeURIComponent(code).replace(/\+/g, " "));
	
	var len = code.length;
	for (var i=0; i<len; i++) {
		if (code[i].codeId == codeId) {
			return code[i].codeNm;
		}
	}
});


Vue.filter('dateFormat', function (val, format) {
	if (!format) format = "YYYY-MM-DD";
	return moment(val).format(format);
});

Vue.filter('dateTimeFormat', function (val, format) {
	if (!format) format = "YYYY-MM-DD HH:mm";
	return moment(val).format(format);
});

Vue.filter('phone', function (phone) {
	if (!phone) return;
	phone = phone.replace(/[^0-9]/g, '');
	return phone.replace(/(01\d{1}|02|0\d{1,2})(\d{3,4})(\d{4})/, "$1-$2-$3");
}); 

Vue.filter('number', function (val, defVal) {
	if (val == null) return addComma(defVal);
	else return addComma(val);
});