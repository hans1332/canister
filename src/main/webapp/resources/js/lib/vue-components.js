// 페이징
var paginator = Vue.component('paginator', {
	template: "<div class=\"pagination\" v-show=\"totRows\">"
			+ "	<ol>"
			+ "		<li v-if=\"page > 1\" class=\"btn_first\"><button @click.stop.prevent=\"goPage(1)\">처음</button></li>"
			+ "		<li v-if=\"page >= 11\" class=\"btn_prev\"><button @click.stop.prevent=\"goPage(startPageBlock-1)\">이전</button></li>"
			+ "		<li v-for=\"n in paginationRange\" class=\"num \" :class=\"{on:n==page}\"><button @click.stop.prevent=\"goPage(n)\">{{ n }}</button></li>"
			+ "		<li v-if=\"endPageBlock < totPage\" class=\"btn_next\"><button @click.stop.prevent=\"goPage(endPageBlock+1)\">다음</button></li>"
			+ "		<li v-if=\"page < totPage\" class=\"btn_last\"><button @click.stop.prevent=\"goPage(totPage)\">마지막</button></li>"
			+ "	</ol>"
			+ "</div>"
	, props: {
		goFn: {
			type: Function
			, default: function() {}
		}
		, initRows: {
			type: Number
			, default: 50
		}
	}
	, data: function() {
		return {
			page: 1
			, totRows: 0
			, rows: 50
			, block: 10
		}
	}
	, computed: {
		paginationRange: function() {
			var range = new Array();
			for (var i = this.startPageBlock; i <= this.endPageBlock; i++)
				range.push(i);
			return range;
		}
		, startPageBlock: function() {
			return Math.floor((this.page-1) / this.block) * this.block + 1;
		}
		, endPageBlock: function() {
			var endBlock = this.startPageBlock + this.block - 1;
			if (endBlock > this.totPage)
				return this.totPage;
			else
				return endBlock;
		}
		, totPage: function() {
			return Math.ceil(this.totRows / this.rows);
		}
		, startRow: function() {
			return (this.page * this.rows) - this.rows;
		}
		, endRow: function() {
			return this.startRow + this.rows - 1;
		}
		, pageNumAsc: function() {
			return (this.page - 1) * this.rows + 1;
		}
		, pageNumDesc: function() {
			return this.totRows - ((this.page - 1) * this.rows);
		}
	}
	, mounted: function() {
		this.rows = Number(this.initRows);
	}
	, methods: {
		goPage: function(page) {
			this.page = page;
			if(typeof(this.goFn) == "function"){
				this.goFn(page);
			}
		}
		, pagingList: function(list) {
			this.totRows = list.length;
			var startRow = this.startRow;
			var endRow = this.endRow;
			return list.filter(function(obj, idx) {
				return (idx >= startRow && idx <= endRow);
			});
		}
		, setPagingInit: function(obj) {
			if (obj) {
				this.rows = obj.rows;
				this.block = obj.block;
				this.totRows = obj.totRows;
				this.page = obj.page;
			}
		} 
		, getInitData: function(){
			return this._data;
		}
	}
});