<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>title</title>
    <tiles:insertAttribute name="resource" />
</head>
<body>
    <div class="container">
        <tiles:insertAttribute name="top" />
        <tiles:insertAttribute name="body" />
    </div>
</body>
</html>