<meta charset="UTF-8">
<meta name="robots" content="noindex, nofollow">

<link rel="shortcut icon" href="/images/favicon.ico"/>

<link type="text/css" rel="stylesheet" href="/css/reset.css">
<link type="text/css" rel="stylesheet" href="/css/bootstrap.css">

<script type="text/javascript" src="/js/lib/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/js/lib/jquery.form.min.js"></script>
<script type="text/javascript" src="/js/lib/bootstrap.js"></script>
<script type="text/javascript" src="/js/lib/vue.min.js"></script>
<script type="text/javascript" src="/js/lib/vue-components.js"></script>
<script type="text/javascript" src="/js/lib/vue-filter.js"></script>
