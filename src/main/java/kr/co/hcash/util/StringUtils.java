package kr.co.hcash.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StringUtils extends org.apache.commons.lang3.StringUtils {

	public static boolean isEmpty(String str){
		return str == null || str.equals("");
	}
	public static boolean isNotEmpty(String str){
		return !isEmpty(str);
	}
	public static boolean isEmpty(Object obj){
		return obj == null;
	}
	public static boolean isNotEmpty(Object obj){
		return !isEmpty(obj);
	}
	public static String toString(Object obj , String defaultValue){return isEmpty(obj) ? defaultValue : obj.toString();}
	public static String toString(Object obj){
		return toString(obj , "");
	}

}
