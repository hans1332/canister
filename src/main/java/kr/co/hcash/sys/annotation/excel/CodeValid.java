package kr.co.hcash.sys.annotation.excel;

import org.springframework.web.bind.annotation.Mapping;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({java.lang.annotation.ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Mapping
public @interface CodeValid {

	/**
	 * 부모코드
	 * @return
	 */
	public abstract String codeType() default "";

	/**
	 * 코드명
	 * @return
	 */
	public abstract String codeName() default "0";

}
