package kr.co.hcash.sys.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Field;
import java.util.Properties;

@Slf4j
@Intercepts(
		{
			@Signature(type = Executor.class , method= "update" , args={MappedStatement.class , Object.class})
			,@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})
			,@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
		}
)
public class QueryInterceptor implements Interceptor {

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		log.debug("intercept");

		Object[] args = invocation.getArgs();
		MappedStatement ms = (MappedStatement)args[0];
		Object param = (Object) args[1];
		BoundSql sql = ms.getBoundSql(param);


		log.debug("###################################################");
		log.debug(invocation.getMethod().getName());
		log.debug(ms.getId());
		log.debug(sql.getSql());
		printLog(param);
		log.debug("###################################################");


		return invocation.proceed();
	}

	@Override public Object plugin(Object target) { return Plugin.wrap(target, this); }
	@Override public void setProperties(Properties properties) { }

	void printLog(Object param){
		if(param != null){
			Class c = param.getClass();
			Field[] fields = c.getDeclaredFields();

			log.debug(fields.length + "");
			for(int i = 0; i < fields.length ; i++){
				log.debug("feild[] : " + fields[i]);
			}

			Class superc = c.getSuperclass();
			Field[] superFields = superc.getDeclaredFields();
			log.debug("super =>");
			log.debug(superFields.length + "");
			for(int i = 0; i < superFields.length ; i++){
				log.debug("superFields[] : " + superFields[i]);
			}
		}
	}

	void setSupperFields(Object param){
		if(param != null){
			Class c = param.getClass();
			Field[] fields = c.getDeclaredFields();
			Class superc = c.getSuperclass();
			if(superc != null){
				Field[] superFields = superc.getDeclaredFields();
				for(int i = 0; i < superFields.length ; i++){
				}
			}
		}
	}


}
