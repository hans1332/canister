package kr.co.hcash.biz.model;

import lombok.Data;

@Data
public class Sample extends BaseModel {
    int seq;
    String comment;
}
