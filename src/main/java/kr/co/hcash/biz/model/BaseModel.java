package kr.co.hcash.biz.model;

import lombok.Data;

import java.util.Date;

@Data
public class BaseModel {

    String regId;
    Date regDate;
}
