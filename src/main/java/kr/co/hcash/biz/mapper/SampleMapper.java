package kr.co.hcash.biz.mapper;

import kr.co.hcash.biz.model.Sample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SampleMapper {

    List<Sample> selectTest();

}