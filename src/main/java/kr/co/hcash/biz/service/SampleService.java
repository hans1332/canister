package kr.co.hcash.biz.service;

import kr.co.hcash.biz.mapper.SampleMapper;
import kr.co.hcash.biz.model.Sample;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SampleService {

    @Autowired
    private SampleMapper mapper;

    public List<Sample> selectTest(){
        return mapper.selectTest();
    }

}
