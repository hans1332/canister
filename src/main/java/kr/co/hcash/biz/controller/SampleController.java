package kr.co.hcash.biz.controller;

import kr.co.hcash.biz.model.Sample;
import kr.co.hcash.biz.service.SampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@Controller("sampleController")
public class SampleController {

	@Autowired
	SampleService sampleService;

	@RequestMapping("/")
	public String index(Model model) {
		return "redirect:/main";
	}

	@RequestMapping("/main")
	public void main(Model model) {
		List<Sample> list = sampleService.selectTest();
		model.addAttribute("list" , list);
	}

	@RequestMapping("/main/login")
	public void login(Model model) {
	}
	
}
